package ru.dolbak.graphics

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.ImageView
import androidx.appcompat.app.AppCompatActivity


class MainActivity : AppCompatActivity() {
    private val picIds = arrayOf(R.drawable.cat, R.drawable.racoon, R.drawable.parrot, R.drawable.dog)
    private var picNow = 0
    private lateinit var iv: ImageView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val sharedPref: SharedPreferences = getPreferences(MODE_PRIVATE)
        picNow = sharedPref.getInt("picnow", 0)
        iv = findViewById(R.id.imageView)
        iv.setImageResource(picIds[picNow])
        Log.d("NIKITA", picNow.toString())
    }

    fun onClick(view: View) {
        picNow++
        picNow %= picIds.size
        val sharedPref = getPreferences(Context.MODE_PRIVATE)
        val editor = sharedPref.edit()
        editor.putInt("picnow", picNow)
        editor.apply()
        iv = findViewById(R.id.imageView)
        iv.setImageResource(picIds[picNow])
        Log.d("NIKITA", picNow.toString())
    }
}